import axios, {AxiosRequestConfig} from "axios";
import * as XLSX from 'xlsx';
import * as fs from 'fs';

interface bodyInterface {
    name: string;
    date_of_birth: string;
    address: string;
    phone_number: string;
}

// Function to read the Excel file
function readExcelFile(filePath: string) {
    try {
        const workbook = XLSX.readFile(filePath);
        const sheetNames = workbook.SheetNames;
        const allData = [];

        for (const sheetName of sheetNames) {
            const worksheet = workbook.Sheets[sheetName];
            const sheetData = XLSX.utils.sheet_to_json(worksheet);
            allData.push({sheetName, data: sheetData});
        }

        return allData;
    } catch (error) {
        throw new Error(`Error reading Excel file: ${error}`);
    }
}

// Function to make API requests for a given sheet's data
async function makeApiRequests(data: any[], apiUrl: string, apiKey: string) {
    // Create Axios request configuration with headers
    const axiosConfig: AxiosRequestConfig = {
        headers: {
            'NRC-API-KEY': apiKey,
        },
    };
    const allRequestBody: bodyInterface[] = [];
    for (const row of data) {
        for (const sheet of row.data) {
            try {
                const name: string = sheet.name;
                const date_of_birth: string = sheet.date_of_birth;
                const address: string = sheet.address;
                const phone_number: string = sheet.phone_number;

                if (isEmptyField(name) || isEmptyField(date_of_birth) || isEmptyField(address) || isEmptyField(phone_number)) {
                    console.error('Invalid data in row, skipping API request: ', sheet);
                    continue;
                }

                const body: bodyInterface = {
                    'name': sheet.name,
                    'date_of_birth': sheet.date_of_birth,
                    'address': sheet.address,
                    'phone_number': sheet.phone_number
                };

                // create an array of all project participants data
                allRequestBody.push(body);

            } catch (error) {
                throw new Error(`Error formating data: ${error}`);
            }

        }
    }

    try {
        // Make an HTTP POST request to the API
        const {data} = await axios.post(apiUrl, allRequestBody, axiosConfig);
        console.log('API response for row:', data);
    } catch (error) {
        throw new Error(`Error making API request: ${error}`);
    }
}

// Checks if the field is a valid string
function isEmptyField(input: any): boolean {
    if (typeof input === 'undefined' || input === null) {
        return true; // Input is undefined or null, consider it empty
    }

    const input_str = String(input).trim(); // Convert input to a string and trim it
    return input_str === '';
}


// Command-Line arguments with default values
const excelFilePath = process.argv[2] || 'files/project_participants.xlsx'; // The path to the Excel file
const apiUrl = process.argv[3] || 'https://egoless-dev.free.beeceptor.com'; // My sample API endpoint URL


const apiKey: string = 'NRC-API-KEY-STRING'; // Add the API KEY
// Check if the required command-line arguments are provided
if (!excelFilePath || !apiUrl) {
    console.log('Usage: npx ts-node excel-to-api.ts <ExcelFilePath> <API_URL>');
    process.exit(1);
}

// Check if the Excel file exists
if (!fs.existsSync(excelFilePath)) {
    console.error('Excel file does not exist:', excelFilePath);
    process.exit(1);
}

// Read the Excel file and make API requests
const excelData = readExcelFile(excelFilePath);
makeApiRequests(excelData, apiUrl, apiKey);